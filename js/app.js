
import { render } from 'react-dom';
import React, { Component } from 'react';
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import { Link, Router, Route, IndexRoute, hashHistory } from 'react-router';
import configureStore from './configureStore';
import App from './components/App';
import Overview from './components/overview/Overview';
import Whois from './components/whois/Whois';
import Graphs from './components/graphs/Graphs';

const store = configureStore();
window.store = store;
const Root = ({store}) => (
	<Provider store={store}>
		<Router history={hashHistory}>
			<Route path='/' component={App} >
				<IndexRoute component={Overview} />
				<Route path='/whois' component={Whois} />
				<Route path='/graphs' component={Graphs} />
			</Route>
		</Router>
	</Provider>
)

render(<Root store={store} />, document.getElementById('root'));