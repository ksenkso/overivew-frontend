import React, { Component } from 'react';
import { connect } from 'react-redux';
import GraphPeriodButton from './GraphPeriodButton';

class LineGraph extends Component {

	constructor() {
		super();

		this._periods = [1, 2, 3, 8, 24];
		this._margin = {
			top: 20,
			right: 20,
			bottom: 40,
			left: 60
		}
		this._dims = {
			width: 320 - this._margin.left - this._margin.right,
			height: 210 - this._margin.top - this._margin.bottom
		}
		this._dateMapping = {
			1: {
				domain: [new Date(Date.now() - 3600*1000), new Date(Date.now())],
				ticks: [d3.time.minute, 6]
			},
			2: {
				domain: [new Date(Date.now() - 2*3600*1000), new Date(Date.now())],
				ticks: [d3.time.minute, 12]
			},
			3: {
				domain: [new Date(Date.now() - 3*3600*1000), new Date(Date.now())],
				ticks: [d3.time.minute, 15]
			},
			8: {
				domain: [new Date(Date.now() - 8*3600*1000), new Date(Date.now())],
				ticks: [d3.time.hour, 1]
			},
			24: {
				domain: [new Date(Date.now() - 24*3600*1000), new Date(Date.now())],
				ticks: [d3.time.hour, 3]
			}
		}
	}

	draw(data, period, msg) {
		if (data[0]) {
		const keys = Object.keys(data[0]);		
		let colorMapping = {};
		const colors = ['#1a870d', '#870d67', '#b67411'];
		keys.forEach((key, i) => {
			colorMapping[key] = colors[i];
		});
		let dataMapping = {};
		keys.forEach(key => dataMapping[key] = []);
		data.forEach(
			object => keys.forEach(
				key => dataMapping[key].push(+object[key])
			)
		);
		
		//const barWidth = this._dims.width/data.length;
		let axs = this.refs['graph'].querySelectorAll('g');
		if (axs.length) {
			for (let i = axs.length - 1; i >= 0; i--) {
				axs[i].remove();
			}
		}
		let paths = this.refs['graph'].querySelectorAll('path');
		if (paths.length) {
			for (let i = paths.length - 1; i >= 0; i--) {
				paths[i].remove();
			}
		}

		const maxHeight = d3.max(Object.keys(data).map(type => d3.max(type)));
		
		const x = d3.time.scale()
					.domain(this._dateMapping[period].domain)
					.range([0, this._dims.width]);
		//const x = d3.scale.linear().domain([0, 20]).range([0, this._dims.width]);

		const y = d3.scale.linear().domain([0, 1]).rangeRound([this._dims.height-20, 0	]);

		const line = d3.svg.line()
			.x((d, i) => x(+this._dateMapping[period].domain[0] + ((+Date.now() - this._dateMapping[period].domain[0]) / dataMapping[keys[0]].length) * i) )
			.y(d => y(d));

		const xAxis = d3.svg.axis()
					.scale(x)
					.orient("bottom")
					.ticks(this._dateMapping[period].ticks[0], this._dateMapping[period].ticks[1])
					.tickFormat(d3.time.format('%H:%M'))
					.tickSize(1,1);

		const yAxis = d3.svg.axis()
					.scale(y).orient("left")
					.tickFormat(d3.format(".0%"))
					.ticks(5)

		let chart = d3.select(this.refs['graph'])
		.attr('width', `${this._dims.width}px`)
		.attr('height', `${this._dims.height+66+60}px`);

		chart.append("svg:g")
		.attr("class", "x axis")
		.attr("transform", `translate(0,${this._dims.height})`) //${(barWidth-1)/2}
		.call(xAxis)
		.selectAll("text")  
			.style("text-anchor", "end")
			.style('font-size', '9px')
			.attr("dx", "-.8em")
			.attr("dy", ".15em")
			.attr("transform", "rotate(-65)" );			

		chart.append('svg:g')
		.attr('class', 'y axis')
		.attr('transform', 'translate(0,20)')
		.call(yAxis)
		.selectAll("text")  
			.style("text-anchor", "start")
			.style('font-size', '9px')
			.attr('x', '0px')
			.attr("dy", ".15em")
		const legend = chart.append('svg:g')
							.attr('class', 'legend');
		Object.keys(dataMapping).forEach(
			(type, index) => {
				chart.append('svg:path')
						.attr('d', d => line(dataMapping[type]))
						.attr('stroke-width', '2px')
						.attr('stroke', colorMapping[type])
						.attr('transform', 'translate(19,20)');
				let group = legend.append('svg:g')
								.attr('transform', `translate(0, ${this._dims.height+50 + 20*(index)})`)
				group.append('svg:rect')
					.attr('width', '20px')
					.attr('height', '10px')
					.attr('fill', colorMapping[type])
					
				group.append('svg:text')
					.text(type.split('_').join(' '))
					.attr('transform', `translate(25, 7)`)
					.attr('font-size', '9px')
			}
		);

		} else return;
	}

	
	componentWillUpdate(nextProps) {
		this._data = nextProps.graphs[nextProps.name].data
		this._period = nextProps.graphs[nextProps.name].period;
		this.draw(this._data, this._period, 'props');
		this.refs.graph.style.opacity = 1;
	}

	componentDidMount() {
		this.refs.graph.style.opacity = 0;
		this._data = this.props.graphs[this.props.name].data
		this._period = this.props.graphs[this.props.name].period;
		//this.draw(this._data, this._period, 'didMount');
	}

	render() {
		return (
			<div class="graph__container">
				<h3 class="graph__name" style={{textAlign:'center'}} >{this.props.name.split('_')[1]}</h3>

				<svg ref="graph" class="graph"></svg>
				<ul class="graph__periods">
					{this._periods.map(
						period => (
							<GraphPeriodButton 
								active={this._period == period} 
								type={this.props.name} 
								period={period} 
							/>)
						)}
				</ul>
			</div>
		)
	}
}

const mapState = (state) => ({
	graphs: state.graphs
})

LineGraph = connect(mapState)(LineGraph);

export default LineGraph;