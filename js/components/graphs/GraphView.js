import React, { Component } from 'react';
import { connect } from 'react-redux';
import GraphTypeSwitcher from './GraphTypeSwitcher';

class GraphView extends Component {

	getAllActiveGraphs() {
		return Object.keys(this.props.graphs)
							.filter(graph => this.props.graphs[graph].active);
	}

	getActiveGraph() {
		const visible = this.getAllActiveGraphs();
		return this.props.children.filter(graph => visible.includes(graph.ref))[0];
	}

	componentWillReceiveProps() {
		this._activeGraph = this.getActiveGraph();
		//console.log(this._activeGraph);
	}

	componentWillMount() {
		this._graphs = this.props.children.map(graph => graph.ref);
		this.getAllActiveGraphs();
		this._activeGraph = this.getActiveGraph();
		this._inactiveGraph = this.props.children.filter(graph => graph !== this._activeGraph)[0];
		console.log(this._inactiveGraph);

	}

	render() {
		return (
		<div class="graph__view">
			<GraphTypeSwitcher active={this._activeGraph.ref} inactive={this._inactiveGraph.ref} names={this._graphs} />
			{this._activeGraph}
		</div>
		)
	}
}

const mapState = (state) => ({
	graphs: state.graphs
});

GraphView = connect(mapState)(GraphView);

export default GraphView;