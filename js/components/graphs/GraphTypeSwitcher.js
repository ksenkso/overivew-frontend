import React, { Component } from 'react';
import { connect } from 'react-redux';
import { updateActiveGraphType } from '../actionCreators';
class GraphTypeSwitcher extends Component {

	onClick(active, inactive) {
		this.props.onClick(active, inactive);
	}

	render() {
		return (
			<div class="graph__switcher">
				{this.props.names.map(
					(name, i , names) => (
						<button 
							class={this.props.active == name ?
							 'graph__type graph__type--active' : 
							 'graph__type'} 
							onClick={this.onClick.bind(this, name, names[i+1] || names[i-1])}				 
						 >
						 {name.split('_')[1]}
						 </button>)
				)}
			</div>
		)
	}
}

GraphTypeSwitcher = connect(null, {onClick: updateActiveGraphType})(GraphTypeSwitcher)

export default GraphTypeSwitcher;