import React, { Component } from 'react';
import { connect } from 'react-redux';
import GraphPeriodButton from './GraphPeriodButton';

class StackedGraph extends Component {	

	constructor() {
		super();
		this._periods = [1, 2, 3, 8, 24];
		this._margin = {
			top: 20,
			right: 20,
			bottom: 40,
			left: 60
		}
		this._dims = {
			width: 320 - this._margin.left - this._margin.right,
			height: 180 - this._margin.top - this._margin.bottom
		}
		this._dateMapping = {
			1: {
				domain: [new Date(Date.now() - 3600*1000), new Date(Date.now())],
				ticks: [d3.time.minute, 6]
			},
			2: {
				domain: [new Date(Date.now() - 2*3600*1000), new Date(Date.now())],
				ticks: [d3.time.minute, 12]
			},
			3: {
				domain: [new Date(Date.now() - 3*3600*1000), new Date(Date.now())],
				ticks: [d3.time.minute, 15]
			},
			8: {
				domain: [new Date(Date.now() - 8*3600*1000), new Date(Date.now())],
				ticks: [d3.time.hour, 1]
			},
			24: {
				domain: [new Date(Date.now() - 24*3600*1000), new Date(Date.now())],
				ticks: [d3.time.hour, 3]
			}
		}
	}

	draw(data, period) {
		
		const barWidth = this._dims.width/data.length;
		let elems = this.refs['graph'].querySelectorAll('g');
		if (elems.length) {
			for (let i = elems.length - 1; i >= 0; i--) {
				elems[i].remove();
			}
		}
		
		const color = d3.scale
		.quantize()
		.domain([0, 4])
		.range(['#62df14', '#e3f01a', '#f0ae1a', '#f42e2e']);

		const x = d3.time.scale()
					.domain(this._dateMapping[period].domain)
					.range([0, this._dims.width]);

		const y = d3.scale.linear().rangeRound([this._dims.height, 0]);

		const xAxis = d3.svg.axis()
					.scale(x)
					.orient("bottom")
					.ticks(this._dateMapping[period].ticks[0], this._dateMapping[period].ticks[1])
					.tickFormat(d3.time.format('%H:%M'))
					.tickSize(1,1);

		const yAxis = d3.svg.axis()
					.scale(y).orient("left")
					.tickFormat(d3.format(".0%"));

		let barsData = [];
		for (var i = 0; i < data.length; i++) {
			let y0 = 0, y1, height, total = 0, group = {};
			let machinesTotal = data[i].reduce((p,c)=>p+ +c,0);
			barsData[i] = [];
			for (var j = 0; j < data[i].length; j++) {
				y1 = y((data[i].slice(0, j).reduce((p, c) => p+ +c, 0)+ +data[i][j])/machinesTotal);
				y0 += y;
				height = ((this._dims.height/machinesTotal) * data[i][j]).toFixed();
				total += +height;
				group = {height, y:y1, color:color(j)};
				barsData[i].push(Object.assign({}, group));
			}
		}

		let chart = d3.select(this.refs['graph'])
		.attr('width', `${this._dims.width}px`)
		.attr('height', `${this._dims.height+33}px`);
		let bars = chart.selectAll('g')
		.data(barsData)
		.enter().append('g')
			.attr('transform', (d, i) => `translate(${(barWidth)*i}, 0)`);
		let groups = bars.selectAll('rect')
		.data(d => d)
		.enter().append('rect')
			.attr('height', d => `${d.height}px`)
			.attr('y', d => `${d.y}`)
			.attr('width', barWidth-1)
			.attr('fill', d => d.color)
		chart.append("g")
		.attr("class", "x axis")
		.attr("transform", `translate(0,${this._dims.height})`) //${(barWidth-1)/2}
		.call(xAxis)
		.selectAll("text")  
			.style("text-anchor", "end")
			.style('font-size', '9px')
			.attr("dx", "-.8em")
			.attr("dy", ".15em")
			.attr("transform", "rotate(-65)" );;
	}

	componentWillReceiveProps(nextProps) {
		this._data = nextProps.graphs[nextProps.name].data
		this._period = nextProps.graphs[nextProps.name].period;
		this.draw(this._data, this._period);
		this.refs.graph.style.opacity = 1;
	}

	componentDidMount() {
		this.refs.graph.style.opacity = 0;
		this._data = this.props.graphs[this.props.name].data
		this._period = this.props.graphs[this.props.name].period;
		this.draw(this._data, this._period);
	}

	render() {
		return (
			<div class="graph__container">
				<h3 class="graph__name">{this.props.name.split('_').join(' ')}</h3>

				<svg ref="graph" class="graph"></svg>
				<ul class="graph__periods">
					{this._periods.map(
						period => (
							<GraphPeriodButton 
								active={this._period == period} 
								type={this.props.name} 
								period={period} 
							/>)
						)}
				</ul>
			</div>
		)
	}
}

const mapState = (state) => ({
	graphs: state.graphs
});

StackedGraph = connect(mapState)(StackedGraph);

export default StackedGraph;