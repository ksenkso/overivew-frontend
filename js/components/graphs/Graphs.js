import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchGraphs } from '../actions';
import StackedGraph from './StackedGraph';
import GraphView from './GraphView';
import LineGraph from './LineGraph';

class Graphs extends Component {

	constructor() {
		super();
		this._data = [
			[4, 5, 7, 3], 
			[3, 7, 7, 2], 
			[7, 5, 7, 0], 
			[4, 5, 7, 3], 
			[4, 5, 7, 3], 
			[4, 7, 5, 3],
			[4, 5, 7, 3], 
			[7, 5, 7, 0],		
		];
	}

	componentWillMount() {
		Object.keys(this.props.graphs).forEach(graph => {
			this.props.onMount(graph, this.props.graphs[graph].period);			
		})
	}

	componentDidMount() {
		Object.keys(this.props.graphs).forEach(graph => {
			this.props.onMount(graph, this.props.graphs[graph].period);			
		})
	}

	render() { 
		return (
			<div class="page ">
				<div>
					<h3>availability</h3>
					<LineGraph name="avail_hardware" ref="avail_hardware"/>
				</div>
				<div class="">
					<h3>performance</h3>
					<GraphView>
						<StackedGraph name="cpu_time" ref="cpu_time" />
						<StackedGraph name="cpu_load" ref="cpu_load" />
					</GraphView>
					<GraphView>
						<StackedGraph name="memory_ram" ref="memory_ram" />
						<StackedGraph name="memory_swap" ref="memory_swap" />
					</GraphView>
					<StackedGraph name="disk_usage" ref="disk_usage" />
				</div>
			</div>
		)
	}
}

const mapState = (state) => ({
	graphs: state.graphs,
});

Graphs = connect(mapState, { onMount: fetchGraphs })(Graphs);

export default Graphs;