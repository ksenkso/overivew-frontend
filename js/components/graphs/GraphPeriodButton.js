import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchGraphs } from '../actions';

class GraphPeriodButton extends Component {
	
	onClick() {
		const button = this.refs.button;
		button.classList.add('graph__period--active');
		[].filter.call(
			button.parentNode.children,
			(a, b) =>a !== button )
			.forEach(btn => btn.classList.remove('graph__period--active'));

		this.props.onClick(this.props.type, this.props.period);
	}

	render() {
		return (
		<li 
			ref="button" 
			onClick={this.onClick.bind(this)} 
			class={ this.props.active ?
						"graph__period graph__period--active" :
						"graph__period" }
		>
		{`${this.props.period}h`}
		</li>
		)
	}
}

GraphPeriodButton = connect(null, {onClick: fetchGraphs})(GraphPeriodButton);

export default GraphPeriodButton;