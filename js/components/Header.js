import React, { Component } from 'react';
import { Link, IndexLink } from 'react-router';

const Header = (props) => (
	<header class="header">
		<nav class="nav">
			<ul class="nav__container">
				<li class="nav__item">
					<IndexLink to="/" class="nav__link" activeClassName="nav__link--active" >Overview</IndexLink>
				</li>
				<li class="nav__item">
					<Link to="/graphs" class="nav__link" activeClassName="nav__link--active" >Graphs</Link>
				</li>
				<li class="nav__item">
					<Link to="/whois" class="nav__link" activeClassName="nav__link--active" >Whois</Link>
				</li>
				<li class="nav__item nav__item--logout">
					<a href="/logout.php" class="nav__link">Logout</a>
				</li>
			</ul>
		</nav>
	</header>
)

export default Header;