import * as actionCreators from './actionCreators';

export const fetchOverview = () => {
	return (dispatch) => {
		return fetch('/overview.php')
				.then(response => {
					return response.json()
				})
				.then(res => {
					console.log(res);
					dispatch(actionCreators.overview(res));
				})
	}
}

export const fetchWhoisTable = (filters) => {
	return (dispatch) => {
		console.log('FILTERS: ', filters);
		return fetch(`/whois.php?${filters}`)
				.then(response => response.json())
				.then(response => dispatch(actionCreators.updateTable(response)));
	}
}

export const fetchGraphs = (type = 'all', period = '1') => {
	return (dispatch) => {
		if (type !== 'all') dispatch(actionCreators.updateGraphPeriod(type, period))
		return fetch(`/graphs.php?type=${type}&period=${period}`)
				.then(response => response.json())
				.then(response => dispatch(actionCreators.updateGraphs(response)));
	}
}

