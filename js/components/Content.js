import React, { Component } from 'react';

const Content = (props) => (
	<div class="content">
		{ props.children }
	</div>
)

export default Content;