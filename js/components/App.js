import React, { Component } from 'react';
import Content from './Content';
import { Link } from 'react-router';
import Header from './Header';

const App = (props) => (
<div class="app">
	<Header />
	<Content >
		{props.children}
	</Content>
</div>
)

export default App;