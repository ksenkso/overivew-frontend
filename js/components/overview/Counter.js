import React, { Component } from 'react';

const Counter = ({ name, values, ranges }) => (
	<div class="counter">
		<h3>{name}</h3>
		<ul class="counter__grouplist">
			{
				values.map((group, i) => (
				<li class={`counter__group counter__group--${i}`}>
					<div class="counter__value">
						{ group.length }
					</div>
					<div class="counter__range">
						{ ranges[i] }
					</div>
					<div class="counter__popup">
							{group.length ? (
								<ul class="machinelist">
								{group.map(name => (
									<li class="machinelist__item">{name}</li>
								))}
							</ul>
							) : (
								<div class="machinelist">No hosts</div>
							) }
						</div>
				</li>
				))
			}
		</ul>
	</div>
)

export default Counter;