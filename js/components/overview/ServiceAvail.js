import React, { Component } from 'react';
import { connect } from 'react-redux';
import AvailTable from './AvailTable';

let ServiceAvail = ({ services }) => (
	<div class="avail__group">
		<h3 class="avail__title">services</h3>
		<AvailTable rows={services} />
	</div>
)

const mapState = (state) => ({
	services: state.overview.avail.services,
});

ServiceAvail = connect(mapState, null)(ServiceAvail);

export default ServiceAvail;