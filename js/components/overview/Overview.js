import React, { Component } from 'react';
import { connect } from 'react-redux';
import SectionEnv from './SectionEnv';
import * as actions from '../actions';
import SectionAvail from './SectionAvail';
import SectionPerf from './SectionPerf';
import 'whatwg-fetch';

class Overview extends Component {

	componentDidMount() {
		this.props.onMount();
	}

	compoenntWillMount() {
		this.props.onMount();
	}

	render() {
		return (
			<div class="page">
				<SectionEnv />
				<SectionAvail />
				<SectionPerf />
			</div>
		)
	}
}






Overview = connect(null, {onMount: actions.fetchOverview})(Overview);

export default Overview;
