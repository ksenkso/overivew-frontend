import React, { Component } from 'react';

const AvailTable = ({ rows }) => (
	<table class="table">
		<tbody>
			<tr class="table__caption">
				<td class="table__col table__col--big">Type</td>
				<td class="table__col">Total</td>
				<td class="table__col">Available</td>
				<td class="table__col">Percentage</td>
			</tr>
		
		{rows.map(
			(row, i) => (
			<tr key={i} class="table__row">
				<td class="table__col table__col--big">{row.name}</td>
				<td class="table__col">{row.total}</td>
				<td class="table__col">{row.available}</td>
				<td class="table__col">{100*row.percent}%</td>
			</tr>
			)
		)}
		</tbody>
	</table>
);

export default AvailTable;