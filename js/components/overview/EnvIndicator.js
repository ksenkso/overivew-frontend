import React, { Component } from 'react';
import RoomAlert from './RoomAlert';

const EnvIndicator = (props) => {

let indicatorList = [];

if (props.view !== false) {
	if (typeof props.view === 'string') {
		indicatorList.push(
			(
				<span title="There is a problem" class="indicator__string indicator--high">
					{props.name.toUpperCase()}!
				</span>
			)
		)
			
		
	} else {
		for (let key in props.view) {
			if (props.view.hasOwnProperty(key)) {
				if (props.view[key] !== false) {
					indicatorList.push(<RoomAlert alert={+props.view[key]} name={`${key} ${props.name}`}/>)
				}
			}
		}
	}
	/*indicatorList = props.view.map((alert, i) => {
		if (typeof alert === 'string') {
			return (
				<span title="There is a problem" key={i} class="indicator__string indicator--high">
					{props.name.toUpperCase()}!
				</span>
			)	
		} else if (typeof alert === 'object') {
			return (
				<RoomAlert key={i} alert={alert} name={props.name}/>
			)
		}
	})*/
} else {
	indicatorList = <div class="indicator__placeholder">No alerts</div>
}
	return (
	<div class="indicator__container">
		<h3 class="indicator__name">{props.name}</h3>
		<div class={!indicatorList.length ? "indicator__group--ok" : "indicator__group--problem"} >
			{indicatorList}
		</div>
	</div>
)}

export default EnvIndicator;