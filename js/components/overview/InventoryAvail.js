import React, { Component } from 'react';
import { connect } from 'react-redux';
import AvailTable from './AvailTable';

let InventoryAvail = ({ inventory }) => (
	<div class="avail__group">
		<h3 class="avail__title">inventory</h3>
		<AvailTable rows={inventory} />
	</div>
)

const mapState = (state) => ({
	inventory: state.overview.avail.inventory,
});

InventoryAvail = connect(mapState, null)(InventoryAvail);

export default InventoryAvail;