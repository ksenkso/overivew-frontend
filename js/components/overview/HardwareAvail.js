import React, { Component } from 'react';
import { connect } from 'react-redux';
import AvailTable from './AvailTable';

let HardwareAvail = ({ hardware }) => (
	<div class="avail__group">
		<h3 class="avail__title">hardware</h3>
		<AvailTable rows={hardware} />
	</div>
)

const mapState = (state) => ({
	hardware: state.overview.avail.hardware
});

HardwareAvail = connect(mapState, null)(HardwareAvail);

export default HardwareAvail;