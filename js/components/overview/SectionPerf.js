import React, { Component } from 'react';
import { connect } from 'react-redux';

import CounterSection from './CounterSection';

let SectionPerf = ({cpu, mem, disk}) => (
	<div class="section section--perf">
		<h3 class="section__title">performance</h3>
		<CounterSection counters={cpu} name='Processor'/>
		<CounterSection counters={mem} name='Memory'/>
		<CounterSection counters={disk} name='Disk'/>
	</div>
)

const mapState = (state) => ({
	cpu: state.overview.perf.cpu,
	mem: state.overview.perf.mem,
	disk: state.overview.perf.disk,
});

SectionPerf = connect(mapState, null)(SectionPerf);

export default SectionPerf;