import React, { Component } from 'react';

const RoomAlert = ({ alert, name }) => (
	<span
		title={!!alert.val ? "Too high!" : "Too low!"} 
		class={!!alert.val ? "indicator__object indicator--high" : "indicator__object indicator--low"} 
	><span>{name.toUpperCase()}!</span>
		
	</span>
)

export default RoomAlert;