import React, { Component } from 'react';

import HardwareAvail from './HardwareAvail';
import ServiceAvail from './ServiceAvail';
import InventoryAvail from './InventoryAvail';


const SectionAvail = ({ avail }) => (
	<div class="section section--env">
		<h1 class="section__title">availability</h1>
		<HardwareAvail />
		<ServiceAvail />
		<InventoryAvail />
	</div>
)

export default SectionAvail;
