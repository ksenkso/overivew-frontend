import React, { Component } from 'react';
import Counter from './Counter';

let CounterSection = ({counters, name}) => (
	<div class="counter__section">
		<h3 class="counter__title">{name}</h3>
		{Object.keys(counters).map(name => (
			<Counter 
				name={name} 
				values={counters[name]}
				ranges={name !== 'load' 
					? ['(0 - 25)%', '(25 - 50)%', '(50 - 75)%', '(75 - 100)%']
					: ['(0 - 0.7)', '(0.7 - 1)', '(1 - 5)', ' >= 100']
				}
			/>
		)) 
	}
	</div>
);



export default CounterSection;