import React, { Component } from 'react';
import EnvIndicator from './EnvIndicator';
import { connect } from 'react-redux';

let SectionEnv = ({ env }) => (
	<div class="section section--env">
		<h3 class="section__title">environment</h3>
		{Object.keys(env).map((indicator, i) => (
			<EnvIndicator key={i} name={indicator} view={env[indicator]} />
			)
		)}
	</div>
)

const mapState = (state) => {
	return {
		env: state.overview.env
	}
}

SectionEnv = connect(mapState)(SectionEnv);

export default SectionEnv;
