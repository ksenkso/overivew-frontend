import React, { Component } from 'react';
import WhoisTable from './WhoisTable';
import FilterBox from './FilterBox';

const Whois = (props) => (
	<div class="page page--column">
		<FilterBox />
		<WhoisTable />
	</div>
)

export default Whois;