import React, { Component } from 'react';
import { connect } from 'react-redux';
import FiltersTable from './FiltersTable';
import FilterApply from './FilterApply';

let FilterBox = ({ filters }) => (
	<div class="filter-box">
		<h3 class="filter-box__title">Current filters</h3>
		{Object.keys(filters).some(f => filters[f] ? true : false) 
			? <FiltersTable filters={filters}/> 
			: <div class="filter-box__empty">No filters defined</div>}
		<FilterApply />
	</div>
)

const mapState = (state) => ({
	filters: state.whois.filters
})

FilterBox = connect(mapState)(FilterBox)

export default FilterBox;