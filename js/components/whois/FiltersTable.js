import React, { Component } from 'react';
import { connect } from 'react-redux';
import WhoisRow from './WhoisRow';
import DeleteFilter from './DeleteFilter';

let FiltersTable = ({filters}) => {

	let rows = [];
	rows = Object.keys(filters).map(name => {
				if (filters[name]) {
					return (<WhoisRow 
								data={[`${name}:`,
								 filters[name],
								 <DeleteFilter name={name} />]}
						 	/>)
				}
			})

	return (
	<table class="filter-box__table">
		<tbody>
			{rows}
		</tbody>
	</table>
)};


export default FiltersTable;