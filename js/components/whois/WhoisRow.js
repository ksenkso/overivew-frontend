import React, { Component } from 'react';

let WhoisRow = ({ data }) => (
	<tr class="table__row">
		{data.map(col => (<td class="table__col">{col}</td>))}
	</tr>
);

export default WhoisRow;