import React, { Component } from 'react';
import TableFilter from './TableFilter';


let WhoisCaption = ({ filters, columns }) => (
	<tr class="table__caption">
		{ filters.map(name => (<TableFilter name={name} />)) }
		{ columns.map(col => (<td class="table__col table__col--whois">{col}</td>)) }
	</tr>
)

export default WhoisCaption;