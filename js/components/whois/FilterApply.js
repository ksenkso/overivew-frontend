import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchWhoisTable } from '../actions';

class FilterApply extends Component {

	onClick() {
		let { filters } = this.props;
		//let queryString = Object.keys(filters).reduce((prev, cur) => (prev += `${cur}=${filters[cur]}&`), '');
		let queryString = '';
		let queryParams = [];
		for (let key in filters) {
			if (filters.hasOwnProperty(key)) {
				queryParams.push(`${encodeURIComponent(key)}=${encodeURIComponent(filters[key])}`)
			}
		}
		queryString = queryParams.join('&');
		//queryString = queryString.slice(0, -1);
		// DomainName=&
		this.props.onClick(queryString);
	}

	render() {
		return (
			<div 
				class="filter-apply"
				onClick={this.onClick.bind(this)}
			>
			Search
			</div>
		)
	}
}

const mapState = (state) => ({
	filters: state.whois.filters
}) 

FilterApply = connect(mapState, {onClick: fetchWhoisTable})(FilterApply);

export default FilterApply;