import React, { Component } from 'react';

class GoogleSearchLink extends Component {


	render() {
		const { string } = this.props;
		return <a 
			target="_blank" 
			href={`https://www.google.ru/#newwindow=1&q=${encodeURIComponent(string)}`}
		>{this.props.string}</a>
	}
}

export default GoogleSearchLink;