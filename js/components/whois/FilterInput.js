import React, { Component } from 'react';
import { connect } from 'react-redux';
import { updateFilter } from '../actionCreators';

class FilterInput extends Component {

	constructor(props) {
		super(props);		
		this.show = this.show.bind(this);
		this.hide = this.hide.bind(this);
		this._name = this.props.name;
		this._value = this.props.filters[this._name];
		this.state = {value: this._value};
	}

	componentDidMount() {
		this.setState({value: this._value});
	}
	componentWillReceiveProps({filters}) {
		this.setState({value: filters[this._name]})
	}
	
	show() {
		this.refs.self.classList.add('filter__input--visible');
		this.refs.self.focus();
	}

	hide() {
		this.refs.self.classList.remove('filter__input--visible');
	}

	onBlur(filter) {
		this.hide();
		let fltr = {};
		fltr[filter] = this.refs.self.value;
		this.props.onBlur(fltr);
	}

	onChange(e) {
		this.setState({value: e.target.value})
	}	

	render() {
		return (
			<input 
				onBlur={this.onBlur.bind(this, this._name)}
				ref="self"
				class={'filter__input'}
				type="text" 
				name={this._name}
				value={this.state.value}
				onChange={this.onChange.bind(this)}
			/>
		)
	}
}

const mapState = (state) => ({
	filters: state.whois.filters
})

FilterInput = connect(mapState, {onBlur: updateFilter}, null, {withRef: true})(FilterInput);

export default FilterInput;