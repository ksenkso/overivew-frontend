import React, { Component } from 'react';
import { connect } from 'react-redux';
import WhoisRow from './WhoisRow';
import WhoisCaption from './WhoisCaption';
import GoogleSearchLink from './GoogleSearchLink';


const filters = [
	'Hostname',
	'IP',
	'MAC',
	'User'
];

const columns = [
	'Type',
	'Manufacturer',
	'Model',
	'OS',
	'CPU Type',
	'CPU Freq.',
	'CPU Qty.',
	'RAM'
];



let WhoisTable = ({ rows }) => {
	
	rows = rows.length ? rows.map(row => {
		row[6] = <GoogleSearchLink string={row[6]} />;
		return (<WhoisRow data={row} />)
	}) : <tr></tr>;
	
	return (
	<table class="table">
		<tbody>
			<WhoisCaption 
				filters={filters} 
				columns={columns} 
			/>
			{ rows }
		</tbody>
	</table>
)};

const mapState = (state) => ({
	rows: state.whois.table
})

WhoisTable = connect(mapState, null)(WhoisTable);

export default WhoisTable;