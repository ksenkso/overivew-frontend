import React, { Component } from 'react';
import { connect  } from 'react-redux';
import FilterInput from './FilterInput';

class TableFilter extends Component {

	onClick() {		
		this.refs.input.getWrappedInstance().show();
	}

	render() {
		return (
			<td class="table__col table__col--filter">
				<span class="table__text" onClick={this.onClick.bind(this)} >{`${this.props.name}`}</span>
				<FilterInput 
					ref="input"
					name={this.props.name} 
				/>
				<img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjwhRE9DVFlQRSBzdmcgIFBVQkxJQyAnLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4nICAnaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkJz48c3ZnIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDM2IDMwIiBoZWlnaHQ9IjMwcHgiIGlkPSJMYXllcl8xIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCAzNiAzMCIgd2lkdGg9IjM2cHgiIHhtbDpzcGFjZT0icHJlc2VydmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPjxwb2x5Z29uIGZpbGw9IiMyMzFGMjAiIHBvaW50cz0iMTQsMzAgMjIsMjUgMjIsMTcgMzUuOTk5LDAgMTcuOTg4LDAgMCwwIDE0LDE3ICIvPjwvc3ZnPg==" alt="filter" class="icon--filter" />
			</td>
		)
	}
}


export default TableFilter;