

export const overview = (state = {}, action) => {
		switch (action.type) {
			case 'OVERVIEW_CHANGE':
				return Object.assign({}, state, action.state);
			default: 
				return state;
		}
	}

export const graphs = (state = {}, action) => {
	switch (action.type) {
		case 'UPDATE_GRAPHS': {
			let tmpGraphs = Object.assign({}, state);
			if (!Array.isArray(action.graphs)) {
				console.log(action.graphs);
				for (let graph in action.graphs) {
					tmpGraphs[graph].data = action.graphs[graph].reverse();
				}	
			} 
			
			return Object.assign({}, state, tmpGraphs);
		}
		case 'UPDATE_GRAPH_PERIOD': {
			console.log(action);
			let graph = {[action.graph]: state[action.graph]};
			graph[action.graph].period = action.period;
			let newState = Object.assign({}, state, graph)
			return newState;
		}
		case 'UPDATE_ACTIVE_GRAPH_TYPE': {
			let tmpGraphs = Object.assign({}, state);
			tmpGraphs[action.newInactive].active = false;
			tmpGraphs[action.newActive].active = true;
			return Object.assign({}, state, tmpGraphs);
		}
		default: 
			return state;
	}
}

export const whois = (state = {}, action) => {
	switch (action.type) {
		case 'UPDATE_FILTER': {
			let { filters } = state;
			filters = Object.assign({}, filters, action.filter);
			return Object.assign({}, state, {filters});	
		}
		case 'UPDATE_TABLE': {
			const { table } = action;
			return Object.assign({}, state, { table });
		}
		default:
			return state;
	}
}

