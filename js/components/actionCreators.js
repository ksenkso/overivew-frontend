
export const overview = (state) => ({
	type: "OVERVIEW_CHANGE",
	state
});

export const updateFilter = (filter) => ({
	type: 'UPDATE_FILTER',
	filter
});

export const updateTable = (table) => ({
	type: 'UPDATE_TABLE',
	table
});

export const updateGraphs = (graphs) => ({
	type: 'UPDATE_GRAPHS',
	graphs
});

export const updateGraphPeriod = (graph, period) => ({
	type: 'UPDATE_GRAPH_PERIOD',
	graph,
	period
});

export const updateActiveGraphType = (newActive, newInactive) => ({
	type: 'UPDATE_ACTIVE_GRAPH_TYPE',
	newActive,
	newInactive
});