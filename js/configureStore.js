import {
	createStore,
	applyMiddleware,
	compose,
	combineReducers
} from 'redux';
import thunk from 'redux-thunk';
import { overview, whois, graphs } from './components/reducers';

const initialGraphData = [
					[4, 5, 7, 3], 
					[3, 7, 7, 2], 
					[7, 5, 7, 0], 
					[4, 5, 7, 3], 
					[4, 5, 7, 3], 
					[4, 7, 5, 3],
					[4, 5, 7, 3], 
					[7, 5, 7, 0], 
					[4, 5, 7, 3], 
					[3, 7, 7, 2], 					
				];

const configureStore = () => {

	
	const persistentData = {
		overview: {
			env: {},
			avail: {
				hardware: [],
				services: [],
				inventory: []
			},
			perf: {
				cpu: [],
				mem: [],
				disk: []
			}
		},
		whois: {
			filters:{
				Hostname: '',
				IP: '',
				MAC: '',
				User: ''
			},
			table: []
		},
		graphs: {
			cpu_time: {
				active: true,
				period: 1,
				data: initialGraphData
			},
			cpu_load: {
				active: false,
				period: 1,
				data: initialGraphData
			},
			memory_swap: {
				active: false,
				period: 1,
				data: initialGraphData
			},
			memory_ram: {
				active: true, 
				period: 1,
				data: initialGraphData
			},
			disk_usage: {
				active: true,
				period: 1,
				data: initialGraphData
			},
			avail_hardware: {
				data: []
			},
			network_incoming: {
				data: []
			},
			network_outgoing: {
				data: []
			},
			network_office: {
				data: []
			}
		}
	}	
	
	

	/*const store = createStore(
		changeTab,
		persistentData,
		applyMiddleware(thunk));*/
		const store = applyMiddleware(thunk)(createStore)(combineReducers({overview, whois, graphs}), persistentData);
	return store;
	
}

export default configureStore;