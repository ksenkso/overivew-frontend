const gulp = require('gulp');
const sass = require('gulp-sass');
const webpack = require('webpack-stream');
const livereload = require('gulp-livereload');

gulp.task('sass', () => {
	gulp.src('./sass/*.sass')
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('/var/www/dashboard/css'))
});

gulp.task('scripts', function () {
    gulp.src('js/**/*.js', {read: false})
        .pipe(webpack(require('./webpack.config.js')))
        .pipe(gulp.dest('/var/www/dashboard/js'))
        .pipe(livereload({start: true}));
});



gulp.task('default', () => {
	livereload.listen({
		basePath: '/var/www/dashboard',
		port: 35729,
		host: 'localhost'
	});
	gulp.watch('./sass/*.sass', ['sass']);
	gulp.watch('js/**/*.js', ['scripts']);
})