const path = require('path');
const webpack = require('webpack');
const NODE_ENV = process.env.NODE_ENV || 'development';
module.exports = {
    entry:  [path.join(__dirname, 'js/app.js')],
    output: {
        path: path.join(__dirname, 'public/build'),
        publicPath: '/',
        filename: 'app.js'
    },
    devtool: (NODE_ENV == 'development') ? 'source-map' : null,
    debug: true,
    module: {
        loaders: [{
            test: /\.js$/,
            include: [
                path.resolve(__dirname, 'js')
            ],
            loader: 'babel-loader',
            query: {
                presets: ['react'],
                plugins: [
                'react-html-attrs',
                'transform-class-properties',
                'transform-decorators-legacy',
                'transform-es2015-modules-commonjs'
                ]
            },
            exclude: /(node_modules|bower_components)/
        }]
    },
    plugins: []

};
