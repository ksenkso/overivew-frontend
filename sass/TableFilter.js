import React, { Component } from 'react';
import { connect  } from 'react-redux';
class TableFilter extends Component {


	render() {
		return (
			<td class="table__filter">
				{this.props.name}
				<input 
					class="filter__input"
					type="text" 
					name={this.props.name.split(' ').join('')}
				/>
			</td>
		)
	}
}

TableFilter = connect()(TableFilter);
export default TableFilter;